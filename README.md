Magnetometer Calibration

Calculates soft iron and hard iron matrices in order to calibrate a magnetometer. Theoretical magnetic field value must be defined.

Function used for fitting can be found [here](https://www.mathworks.com/matlabcentral/fileexchange/24693-ellipsoid-fit) (matlab file exchange).
