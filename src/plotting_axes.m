function [] = plotting_axes(data, real_magnetic_field)
%PLOTTING_AXES Plots the  magnetic field density measured in every axis and
% the measured magnetic field norm along with the theoretical ideal values. 
%   data (array Nx3) : the magnetic field data on every axis and the magnetic
%   field norm.
%   real_magnetic_field (float) : the theoretical correct magnetic field density value.
    norm = vecnorm(transpose(data));
    figure()
    hold on
    
    subplot(4,1,1)
    plot(data(:,1))
    yline(mean(data(:,1)), 'g')
    legend('Value per sample','field mean' )
    grid on
    xlabel('Sample No.')
    ylabel('H_{x}');
    
    subplot(4,1,2)
    plot(data(:,2))
    yline(mean(data(:,2)), 'g')
    legend('Value per sample','field mean' )
    grid on
    xlabel('Sample No.')
    ylabel('H_{y}');
 
    subplot(4,1,3)
    plot(data(:,3))
    yline(mean(data(:,3)), 'g')
    legend('Value per sample','field mean' )
    grid on
    xlabel('Sample No.')
    ylabel('H_{z}');
    
    subplot(4,1,4)
    plot(norm)
    yline(real_magnetic_field, 'r')
    yline(mean(norm), 'g')
    legend('Norm per sample', 'Real field', 'norm mean')
    grid on
    xlabel('Sample No.')
    ylabel('|H|');
    hold off
end

