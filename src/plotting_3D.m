function [] = plotting_3D(data)
%PLOTTING_3D Plots data and their theoretical quadric surface
%   data (array Nx3) : N samples on the x y z axis to plot
    
    figure()
    scatter3(data(:,1), data(:,2), data(:,3)) %plot the ellipsoid
    hold on
    xlabel('x(uT)')
    ylabel('y(uT)')
    zlabel('z(uT)')
    
end

