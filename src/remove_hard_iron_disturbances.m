function [calibrated_data, ellipsoid_center] = remove_hard_iron_disturbances(data_on_every_axis)
%REMOVE_HARD_IRON_DISTURBANCES Fixed hard iror error caused by disturbances
%by calculating the bias and subtracting it.
%   data_on_every_axis (array Nx3) : N samples on the x y z axis for
%   calibration

    [ellipsoid_center, ~, ~, ~, ~]  = ellipsoid_fit_new(data_on_every_axis, '');
    calibrated_data = (data_on_every_axis - transpose(ellipsoid_center));
end