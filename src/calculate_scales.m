function scales = calculate_scales(data)
%CALCULATE_SCALES calculates the 3 axis length of the ellipsoid the data is
% lying on. If data is not forming an ellipsoid, least squares will
% converge to 0.
%   data (array) : The data used for the fitting

    x = data(:,1);
    y = data(:,2);
    z = data(:,3);
    
    H = [x y z -y.^2 -z.^2 ones(length(data),1)];
    w = x.^2;
    
    X = lsqminnorm(H, w);
    
    OSx = X(1) / 2;
    OSy = X(2) / (2 * X(4));
    OSz = X(3) / (2 * X(5));
    
    A = X(6) + OSx^2 + X(4)*OSy^2 + X(5)*OSz^2;
    B = A / X(4);
    C = A / X(5);
    scales = [sqrt(A) sqrt(B) sqrt(C)];
end

