function [calibrated_data, V_bias, W] = calibrate(data_on_every_axis, real_magnetic_field_density)
%CALIBRATE Performs magnetometer data calibration by calculating the soft
%iron and the hard iron matrix. 
%   data_on_every_axis (array Nx3) : N samples on the x y z axis for
%   calibration
%   real_magnetic_field_density (float) : The theoretical magnetic field
%   value in the location the sampling took place.
    [ellipsoid_center, ~, coordinate_system_axis, ~, ~] = ellipsoid_fit_new(data_on_every_axis, '');
    axis_radius = calculate_scales(data_on_every_axis);
    scale_matrix = diag(real_magnetic_field_density./axis_radius);
    axis_missalignment_matrix = coordinate_system_axis';
    V_bias = ellipsoid_center;
    W = scale_matrix*axis_missalignment_matrix;
    
    calibrated_data = (W*(data_on_every_axis' - V_bias))';
end
