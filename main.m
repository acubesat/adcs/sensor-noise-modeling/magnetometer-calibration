close all
clear
clc
addpath('src');
theoretical_magnetic_field_value = 47.1950 % measured in uT

% data should be contain 3 columns (one per axis), each column seperated by
% a space from the others (|i_sample_x i_sample_y i_sample_z|).
raw_data = load('data.txt');

[calibrated_data, V, W] = calibrate(raw_data(:,1:3), ...
                    theoretical_magnetic_field_value);

%Plotting Raw Magnetometer Data
plotting_3D(raw_data(:,1:3))
sgtitle("Raw Magnetometer Data Calibrated")
plotting_axes(raw_data, theoretical_magnetic_field_value)
sgtitle("Magnetic Field Components and Norms plots using raw data")


%Ploting Biased-Fixed Measurements with Soft Iron and Hard Iron Fix
plotting_3D(calibrated_data)
sgtitle('Soft and Hard Iron Calibrated Data Using proposed Algorithm')
plotting_axes(calibrated_data, theoretical_magnetic_field_value)
sgtitle("Magnetic Field Components and Norms using hard and soft iron fixed data - THEORETICAL")
